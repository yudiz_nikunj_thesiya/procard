import Nav from "./components/Nav";
import Home from "./pages/Home";
import { Routes, Route } from "react-router-dom";
import CreateCard from "./pages/CreateCard";

function App() {
	return (
		<div className="flex flex-col w-full h-screen overflow-y-scroll items-center bg-purple-50">
			<Nav />
			<Routes>
				<Route path="/" element={<Home />} />
				<Route path="/createcard" element={<CreateCard />} />
			</Routes>
		</div>
	);
}

export default App;
