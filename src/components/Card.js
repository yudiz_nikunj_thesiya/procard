import Avatar from "react-avatar";
import Twitter from "../images/twitter.svg";
import Instagram from "../images/instagram.svg";
import LinkedIn from "../images/linkedin.svg";
import Github from "../images/github.svg";
import { MdDelete } from "react-icons/md";

const Card = ({
	id,
	first_name,
	last_name,
	job_title,
	instagramId,
	twitterId,
	linkedinId,
	githubId,
	deleteCard,
}) => {
	return (
		<div className="bg-purple-100 w-full rounded-xl hover:shadow-xl hover:cursor-pointer border border-transparent hover:border-purple-300 transition ease-in-out delay-150 overflow-hidden card">
			<div className="w-full h-14 bg-gradient-to-r from-purple-500 to-purple-800 flex items-center justify-between px-4">
				<div></div>
				<div
					className="text-purple-700 bg-purple-100 flex items-center px-3 py-3 self-center text-lg rounded-lg deleteBtn"
					onClick={() => deleteCard(id)}
				>
					<MdDelete />
				</div>
			</div>
			<div className="flex flex-col space-y-3 -mt-5 px-6 pb-5 items-center">
				<div className="w-12 h-12 outline outline-white object-cover rounded-full overflow-hidden shadow-lg">
					<Avatar
						name={`${first_name} ${last_name}`}
						color="#9141D9"
						size="48"
						className="w-full h-full object-contain"
					/>
				</div>
				<div className="flex flex-col items-center">
					<h3 className="text-xl font-bold text-purple-800 line-clamp-1">{`${first_name} ${last_name}`}</h3>
					<h3 className="text-purple-700 text-sm line-clamp-1">{job_title}</h3>
				</div>
				<div className="grid grid-cols-4 md:grid-cols-2 lg:grid-cols-4 gap-4">
					<a
						href={twitterId}
						target="_blank"
						className="socialLink"
						rel="noreferrer"
					>
						<img src={Twitter} alt="Nikunj Thesiya" />
					</a>
					<a
						href={instagramId}
						target="_blank"
						rel="noreferrer"
						className="socialLink"
					>
						<img src={Instagram} alt="Nikunj Thesiya" className="" />
					</a>
					<a
						href={linkedinId}
						target="_blank"
						rel="noreferrer"
						className="socialLink"
					>
						<img src={LinkedIn} alt="Nikunj Thesiya" className="" />
					</a>
					<a
						href={githubId}
						target="_blank"
						rel="noreferrer"
						className="socialLink"
					>
						<img src={Github} alt="Nikunj Thesiya" className="" />
					</a>
				</div>
			</div>
			<div className=""></div>
		</div>
	);
};

export default Card;
