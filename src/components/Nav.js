import React from "react";
import { Link } from "react-router-dom";

const Nav = () => {
	return (
		<div className="flex items-center w-full justify-between bg-purple-700 text-purple-100 shadow-lg px-6 py-4">
			<Link to="/" className="logo font-bold text-3xl">
				ProCard
			</Link>
			<div className="flex space-x-4">
				<Link
					to="/createcard"
					className="px-4 rounded-xl py-3 text-sm border border-transparent bg-purple-100 text-purple-800 hover:text-purple-100 hover:bg-transparent hover:border-purple-100"
				>
					+ Create New Card
				</Link>
			</div>
		</div>
	);
};

export default Nav;
