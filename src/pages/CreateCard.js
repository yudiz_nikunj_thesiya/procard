import React, { useState } from "react";
import { Link } from "react-router-dom";
import { MdClose } from "react-icons/md";
import { useNavigate } from "react-router-dom";

export const cardData = [
	{
		first_name: "Nikunj",
		last_name: "Thesiya",
		job_title: "ReactJS Developer",
		instagram: "https://www.instagram.com/ll_nikunj.thesiya_ll/",
		twitter: "https://www.instagram.com/NikunjThesiya2",
		linkedin: "https://www.linkedin.com/in/nikunjthesiya/",
		github: "https://github.com/NikunjThesiya",
	},
	{
		first_name: "Rikesh",
		last_name: "Patel",
		job_title: "Android Developer",
		instagram: "https://www.instagram.com/",
		twitter: "https://www.instagram.com/",
		linkedin: "https://www.linkedin.com/",
		github: "https://github.com/",
	},
	{
		first_name: "Vandana",
		last_name: "Godhani",
		job_title: "UI/UX Designer",
		instagram: "https://www.instagram.com/",
		twitter: "https://www.instagram.com/",
		linkedin: "https://www.linkedin.com/",
		github: "https://github.com/",
	},
	{
		first_name: "Maulik",
		last_name: "Vamja",
		job_title: "Laravel Developer",
		instagram: "https://www.instagram.com/",
		twitter: "https://www.instagram.com/",
		linkedin: "https://www.linkedin.com/",
		github: "https://github.com/",
	},
	{
		first_name: "Chirag",
		last_name: "Patel",
		job_title: "Android Developer",
		instagram: "https://www.instagram.com/",
		twitter: "https://www.instagram.com/",
		linkedin: "https://www.linkedin.com/",
		github: "https://github.com/",
	},
];

const CreateCard = () => {
	const [cardDetails, setCardDetails] = useState({
		first_name: "",
		last_name: "",
		job_title: "",
		instagram: "",
		twitter: "",
		linkedin: "",
		github: "",
	});

	const history = useNavigate();

	const onInputChange = (e) =>
		setCardDetails({ ...cardDetails, [e.target.name]: e.target.value });

	const onSubmit = (e) => {
		e.preventDefault();
		cardData.push(cardDetails);
		history("/");
	};

	return (
		<div className="absolute top-0 left-0 z-10 flex items-center justify-center w-full h-auto py-24 text-gray-700 bg-purple-500 bg-opacity-50 backdrop-filter backdrop-blur-xl">
			<div className="flex flex-col w-4/5 px-10 py-8 space-y-5 bg-white shadow-xl rounded-2xl">
				<div className="flex items-center justify-between">
					<h4 className="text-xl">Create Card</h4>
					<Link
						to="/"
						className="p-3 text-lg bg-purple-700 text-white rounded-full cursor-pointer"
					>
						<MdClose />
					</Link>
				</div>
				<form className="flex flex-col space-y-3" onSubmit={(e) => onSubmit(e)}>
					<div className="grid grid-cols-1 gap-5 md:grid-cols-2">
						<div className="flex flex-col space-y-3">
							<span>First Name</span>
							<input
								type="text"
								placeholder="John *"
								className="input"
								name="first_name"
								value={cardDetails.first_name}
								onChange={(e) => onInputChange(e)}
								required
							/>
						</div>
						<div className="flex flex-col space-y-3">
							<span>Last Name</span>
							<input
								type="text"
								placeholder="Doe *"
								className="input"
								name="last_name"
								value={cardDetails.last_name}
								onChange={(e) => onInputChange(e)}
								required
							/>
						</div>
					</div>
					<div className="grid grid-cols-1 gap-5">
						<div className="flex flex-col space-y-3">
							<span>Job Title</span>
							<input
								type="text"
								placeholder="Web Developer *"
								className="input"
								name="job_title"
								value={cardDetails.jobtitle}
								onChange={(e) => onInputChange(e)}
								required
							/>
						</div>
					</div>
					<div className="grid grid-cols-1 gap-5">
						<div className="flex flex-col space-y-3">
							<span>Twitter</span>
							<input
								type="url"
								placeholder="Add Twitter profile link.. *"
								className="input"
								name="twitter"
								value={cardDetails.twitter}
								onChange={(e) => onInputChange(e)}
								required
							/>
						</div>
					</div>
					<div className="grid grid-cols-1 gap-5">
						<div className="flex flex-col space-y-3">
							<span>Instagram</span>
							<input
								type="url"
								placeholder="Add Instagram profile link.. *"
								className="input"
								name="instagram"
								value={cardDetails.instagram}
								onChange={(e) => onInputChange(e)}
								required
							/>
						</div>
					</div>
					<div className="grid grid-cols-1 gap-5">
						<div className="flex flex-col space-y-3">
							<span>LinkedIn</span>
							<input
								type="url"
								placeholder="Add LinkedIn profile link..*"
								className="input"
								name="linkedin"
								value={cardDetails.linkedin}
								onChange={(e) => onInputChange(e)}
								required
							/>
						</div>
					</div>
					<div className="grid grid-cols-1 gap-5">
						<div className="flex flex-col space-y-3">
							<span>Github</span>
							<input
								type="url"
								placeholder="Add Github profile link.. *"
								className="input"
								name="github"
								value={cardDetails.github}
								onChange={(e) => onInputChange(e)}
								required
							/>
						</div>
					</div>
					<div className="flex items-center justify-end pt-4">
						<input
							type="submit"
							value="Create Card"
							className="cursor-pointer btn"
						/>
					</div>
				</form>
			</div>
		</div>
	);
};

export default CreateCard;
