import React, { useEffect, useState } from "react";
import Card from "../components/Card";
import { cardData } from "./CreateCard";

const Home = () => {
	const [cards, setCards] = useState([]);

	useEffect(() => {
		setCards(cardData);
	}, []);

	const deleteCard = (id) => {
		const confirmation = window.confirm("You want to delete this card?");
		const result = cards.filter((item, index) => index !== id);
		confirmation && setCards(result);
	};

	return (
		<div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-6 w-full px-8 py-14">
			{cards.map((card, index) => (
				<Card
					key={index}
					first_name={card.first_name}
					last_name={card.last_name}
					job_title={card.job_title}
					twitterId={card.twitter}
					instagramId={card.instagram}
					githubId={card.github}
					linkedinId={card.linkedin}
					id={index}
					deleteCard={deleteCard}
				/>
			))}
		</div>
	);
};

export default Home;
